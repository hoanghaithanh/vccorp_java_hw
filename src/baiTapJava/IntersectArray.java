package baiTapJava;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class IntersectArray {
	private static int[] array1;
	private static int[] array2;
	private static ArrayList<Integer> intersect;
	
	//Khoi tao hai mang chua 2000 so nguyen;
	public void initial(){
		array1 = new int[2000];
		array2 = new int[2000];
		for (int i = 0;i<2000;i++)
		{
			array1[i] = ThreadLocalRandom.current().nextInt(0,10000);
			array2[i] = ThreadLocalRandom.current().nextInt(3000,13000);
		}
		for(int i=0;i<2000;i++)
		{
			System.out.format("%d %d \n",array1[i],array2[i]);
		}
	}
	//Sap xep theo thu tu tu be den lon
	public void sortArray1()
	{
		Arrays.sort(array1);
	}

	public void sortArray2()
	{
		Arrays.sort(array2);
	}
	
	//Tim tap giao
	//Y tuong la tu hai tap da xap xep, bat dau so sanh hai phan 
	//tu i cua mang1 va j cua mang2
	//Neu mang1[i] == mang2[j] thi luu gia tri vao mang giao
	//neu mang1[i] > mang2[j] thi tang j va nguoc lai
	//O thuat toan nay, moi phan tu trong mang duoc duyet duy nhat 1 lan.
	public void findIntersect()
	{

		int i = 0, j=0, moc=0;
		intersect = new ArrayList<Integer>();
		while(i<2000 && j<2000)
		{
			if(array1[i]==array2[j]) {
				intersect.add(array1[i]);
				i++;
				j++;
			}
			
			else{
				if(array1[i]<array2[j])
				{
					do{
						moc = array2[j];
						i++;
					}while(i<2000 && array1[i]<moc);
				}
				else
				{
					do{
						moc=array1[i];
						j++;
					}while(j<2000 && moc>array2[j] );
				}
			}
		}
	}
	
	public static void main(String[] args)
	{
		IntersectArray newIntersect = new IntersectArray();
		newIntersect.initial();
		newIntersect.sortArray1();
		newIntersect.sortArray2();
		newIntersect.findIntersect();
		for(int i=0;i<2000;i++)
		{
			System.out.format("%d %d \n",array1[i],array2[i]);
		}
		for(int i=0;i<intersect.size();i++)
		{
			System.out.println(intersect.get(i));
		}
	}
}
