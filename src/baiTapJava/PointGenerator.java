package baiTapJava;

import java.awt.Point;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class PointGenerator {

 private ArrayList<Point> set1;
 private ArrayList<Point> set2;
 private ArrayList<Point> set3;
 private ArrayList<Point> totalSet;
 
 public void initial()
 {
	 set1 = new ArrayList<Point>();
	 set2 = new ArrayList<Point>();
	 set3 = new ArrayList<Point>();
 }
 
 public void generatePoint()
 {
	 while(set1.size()<8000)
	 {
		Point temp = new Point();
		int x = ThreadLocalRandom.current().nextInt(400,1200);
		int y = 0;
		int yMax = (int) Math.sqrt(160000-Math.pow(x-800, 2)) + 800;
		int yMin = -((int)Math.sqrt(160000-Math.pow(x-800, 2))) + 800;
		if(yMin!=yMax)
		{
			y = ThreadLocalRandom.current().nextInt(yMin,yMax);
		}
		else {
			y = yMin;
		}
		temp.setLocation(x, y);
		//Function to print the distance between generated point and A
		//System.out.println(Math.sqrt(Math.pow(x-800, 2)+Math.pow(y-800, 2)));
		set1.add(temp);
	 }
	 while(set2.size()<10000)
	 {
		Point temp = new Point();
		int x = ThreadLocalRandom.current().nextInt(3500,4500);
		int y = 0;
		int yMax = (int) Math.sqrt(250000-Math.pow(x-4000, 2)) + 800;
		int yMin = -((int)Math.sqrt(250000-Math.pow(x-4000, 2))) + 800;
		if(yMin!=yMax)
		{
			y = ThreadLocalRandom.current().nextInt(yMin,yMax);
		}
		else {
			y = yMin;
		}
		temp.setLocation(x, y);
		//Function to print the distance between generated point and B
		System.out.println(Math.sqrt(Math.pow(x-4000, 2)+Math.pow(y-800, 2)));
		set2.add(temp);
	 }
	 while(set3.size()<12000)
	 {
		Point temp = new Point();
		int x = ThreadLocalRandom.current().nextInt(1800,3000);
		int y = 0;
		int yMax = (int) Math.sqrt(360000-Math.pow(x-4000, 2)) + 2400;
		int yMin = -((int)Math.sqrt(360000-Math.pow(x-4000, 2))) + 2400;
		if(yMin!=yMax)
		{
			y = ThreadLocalRandom.current().nextInt(yMin,yMax);
		}
		else {
			y = yMin;
		}
		temp.setLocation(x, y);
		//Function to print the distance between generated point and B
		System.out.println(Math.sqrt(Math.pow(x-2400, 2)+Math.pow(y-2400, 2)));
		set3.add(temp);
	 }
 }
 
 public void summarizeMultiThread()
 {
	 totalSet = new ArrayList<Point>(30000);
	 Runnable task1 = new Runnable(){

		@Override
		public void run() {
			// TODO Auto-generated method stub
			for(Point p:set1)
			 {
				 totalSet.add(p);
			 }
		}
		 
	 };
	 
	 Runnable task2 = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(Point p:set2)
				 {
					 totalSet.add(p);
				 }
			}
			
	 };
	 Runnable task3 = new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				for(Point p:set3)
				 {
					 totalSet.add(p);
				 }
			}
	 };
	 
	 Thread thread1 = new Thread(task1);
	 
	 Thread thread2 = new Thread(task2);
	 
	 Thread thread3 = new Thread(task3);
	 
	 thread1.start();
	 thread2.start();
	 thread3.start();
	 
 }
 
 public void printFile()
 {
	 File output1 = new File("pointList1.txt");
	 try {
		FileWriter writer1 = new FileWriter(output1);
		for(Point p:set1)
		{
			writer1.write(p.x+": "+p.y+"\r\n");
		}
		writer1.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 
	 
	 File output2 = new File("pointList2.txt");
	 try {
		FileWriter writer2 = new FileWriter(output2);
		for(Point p:set2)
		{
			writer2.write(p.x+": "+p.y+"\r\n");
		}
		writer2.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 
	 
	 File output3 = new File("pointList3.txt");
	 try {
		FileWriter writer3 = new FileWriter(output3);
		for(Point p:set3)
		{
			writer3.write(p.x+": "+p.y+"\r\n");
		}
		writer3.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	 File output = new File("pointList.txt");
	 try {
		FileWriter writer = new FileWriter(output);
		for(Point p:totalSet)
		{
			writer.write(p.x+": "+p.y+"\r\n");
		}
		writer.close();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
 }
 
 public static void main(String[] args)
 {
	 PointGenerator poinGen = new PointGenerator();
	 poinGen.initial();
	 poinGen.generatePoint();
	 poinGen.summarizeMultiThread();
	 poinGen.printFile();
 }
}
