package baiTapJava;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class WordCount {
	private BufferedReader bufferReader;
	//Luu tu va tan so cua no vao Hashmap, tu la key, tan so la value
	public HashMap<String, Integer> wordStore;
	
	//Khoi tao bufferReader
	public void initial()
	{
		try {
			bufferReader = new BufferedReader(new FileReader("src/01.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
	}
	
	//Close reader
	public void closeReader()
	{
			try {
				bufferReader.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Reader already closed!");
				e.printStackTrace();
			}
	}
	
	//Dem tu va luu vao hashmap
	public void countWord()
	{
		String wordToken=null;
		Scanner tokenizer;
		wordStore = new HashMap<String, Integer>();
		try {
			String line = bufferReader.readLine();
			while(line!=null)
			{
				tokenizer = new Scanner(line);
				while(tokenizer.hasNext())
				{
				wordToken = tokenizer.next();
				wordToken = wordToken.toLowerCase();
				StringBuilder result = new StringBuilder();
				for(int i=0; i<wordToken.length(); i++) {
				    char tmpChar = wordToken.charAt( i );
				    if ((tmpChar>='A'&& tmpChar<='Z') || (tmpChar<='z' && tmpChar>='a' )) {
				        result.append( tmpChar );
				    }
				}
				wordToken = result.toString();
				if(wordToken.equals("")||wordToken==null) continue;
				if(wordStore.containsKey(wordToken))
				{
					int oldValue = (int) wordStore.get(wordToken);
					wordStore.put(wordToken, oldValue+1);
				}
				else
				{
					wordStore.put(wordToken, 1);
				}
				}
				line = bufferReader.readLine();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Empty file!");
			e.printStackTrace();
		}
		
	}
	
	//Ghi ra file
	public void writeToFile()
	{
		File f = new File("src/output.txt");
		try {
			FileWriter fileWriter = new FileWriter(f);
			Object[] keySet =  wordStore.keySet().toArray();
			Arrays.sort(keySet);
			for(Object t:keySet)
			{
				fileWriter.write(t+": "+wordStore.get(t)+"\r\n");
			}
			fileWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("Loi!");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args)
	{
		WordCount wordCount = new WordCount();
		wordCount.initial();
		wordCount.countWord();
		wordCount.closeReader();
		wordCount.writeToFile();
	}
	
}
