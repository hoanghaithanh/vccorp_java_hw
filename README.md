# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What are in this repository? ###

* Bai tap java nop lan 1
* Version 1.0
* file IntersectArray.java cai dat chuong trinh tim giao cua hai tap hop, moi tap gom 2000 so nguyen duoc sinh ngau nhien
* file WordCount.java cai dat chuong trinh dem tu cua file "01.txt"
* file WordCountMultiThread.java cai dat chuong trinh dem tu da luong cho bai so 2
* file PointGenerator.java cai dat chuong trinh sinh diem va tron ngau nhien

### What to notice? ###

* O bai 1, em dung ThreadLocalRandom.current().nextInt(a,b) de sinh so nguyen bat ky trong khoang tu a den b, de tim giao cua hai tap hop, em order tung tap hop va tien hanh duyet theo thu tu
* O bai 2, em dung HashMap<String, Integer> de luu cac tu trong van ban va tan so cua chung, cac tu scan duoc em da chuan hoa de thanh tu viet thuong, loai bo cac ky tu dac biet
* O bai 3, em dung HashMap<String, AtomicInteger> de luu tru. Bai nay con han che o cho em phai cho chay Thread.sleep(10000) o ham main vi khong biet chac duoc khi nao cac thread moi ket thuc het
* O bai 4, em dung 3 thread de tron cac diem vao mot mang, tuy nhien no khong duoc "ngau nhien" cho lam a.


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact